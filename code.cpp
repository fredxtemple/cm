#include <iostream>
#include <cmath>

using namespace std;

        
        
 int main() {
 int N=100;
 float eps=1.0;
 for(int i=1 ; i < N; i=i+1) { //The loop is done N times
 eps = eps/2; //Making eps smaller
 float one = 1.0 + eps; //The machine precision is the largest number in a computer computation that can be added to the number stored as 1 without changing the value of that stored 1.
 cout.precision(20); //The precision needs to be 20 to give a very accurate value.
 cout << "eps = " << eps << " one = " << one << endl; //After each loop, the value of eps and "one" is given.
 }
 int M=100;
 double epsdouble =1.0;
 for(int j=1 ; j < M; j=j+1) {
 epsdouble = epsdouble/2;
 double onedouble = 1.0 + epsdouble;
 cout.precision(20);
 cout << "epsdouble = " << epsdouble << " one = " << onedouble << endl; //Same as above - but for double.
 }
 int L=100;
 long double epslongdouble =1.0;
 for(int k=1 ; k < L; k=k+1) {
 epslongdouble = epslongdouble/2;
 long double onelongdouble = 1.0 + epslongdouble;
 cout.precision(20);
 cout << "epslongdouble = " << epslongdouble << " one = " << onelongdouble << endl; //Same as above - but for long double.
 }
 return 0;
}

 Frederick Temple
Frederick Temple
Frederick Temple
Frederick Temple

#include <iostream>
#include <fstream>
#include "mersenne.h" // create new Mersenne Twister 
using namespace std;
int main()
{
MersenneTwister prng(1000); // generate two random 32-bit numbers
ofstream myfile;
myfile.open ("hist.txt");
   for(int i = 0; i <= 100000; i++) {
   double  x = (prng())/4294967295.0; //generates numbers between 0 and 1 (uniformly).
   myfile << x << endl; 
   std::cout << x << std::endl;}

  return 0;
}
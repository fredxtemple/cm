#include <iostream>
#include <fstream>
#include <cmath>
#include "mersenne.h" 
#include <cstdio>
#include <ctime>
using namespace std;
int main()
{
std::clock_t start;
    double duration;

    start = std::clock();

    MersenneTwister prng(1000);
ofstream myfile;
myfile.open ("histq2.txt");
   for(int i = 0; i <= 100000; i++) {
   double x = 1.0 - (prng())/4294967295.0;
   double y = acos(1.0 - 2.0*x); 

   myfile <<  y << endl; }

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    std::cout<<"printf: "<< duration <<'\n';
return 0;
}
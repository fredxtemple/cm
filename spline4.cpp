#include <iostream>
#include <vector>

using namespace std;

void tridag(vector<double> &a, vector<double> &b, vector<double> &c, vector<double> &f, vector<double> &y)

{
    
    
    int i;
    int n = b.size();
    double bet;
    vector<double> gam; //One vector of workspace, gam, is needed
   
    if (b[0] == 0){ throw("Error 1 in tridag"); } //If this happens, then you should rewrte your equations as a set of order N-1 with u1 trivially eliminated
    
    y[0]=f[0]/(bet=b[0]);
    
    for (i=1;i<n;i++) {  //Decomposition and forward substitution
        gam[i]=c[i-1]/bet;
        bet=b[i]-a[i]*gam[i];
        if (bet == 0) { throw("Error 2 in tridag"); } //Algorithm fails
        y[i]=(f[i]-a[i*y[i-1]])/bet;
    }
    
    for (i=(n-2);i>=0;i--) {
        y[i] -= gam[i+1]*y[i+1]; //Backsubstitution
    }
}


 int main()
{

   double A, B, C, D, x_care, xl, yl, yu, xu;


    int i;
    
    double bet;
    vector<double> gam; //One vector of workspace, gam, is needed

 
     
     int n;
     
 vector<double> x(n), y(n), a(n), b(n), c(n), f(n);
     
 n=b.size();
     
//User inputs the data set values
        
 cout<<"Enter the values of x: ";
 cin>>x[i];
	
 cout<<"Enter the values of y: ";
 cin>>y[i];
  
//Input the value of x to interpolate  
 cout<<"Enter the value of x where you wish to interpolate: ";
 cin>>x_care;

     

//For i=1

for (i=1; i < n; i=i+1){

 double x_care = x[i];
 double xl = x[i];
 double xu = x[i+1];
    

 A=(xu-x_care)/(xu-xl);
 B=1-A;
 C=(1/6)*((A*A*A)-A)*(xu-xl)*(xu-xl);
 D=(1/6)*((B*B*B)-B)*(xu-xl)*(xu-xl);

 a[0]=0;
 b[0]=(x[1]-x[0])/3;
 c[0]=(x[1]-x[0])/6;
 f[0]=(y[1]-y[0])/(x[1]-x[0]);


//For i=1 to i= n-2
 for (i=1;i<=(n-2);i=i+1){


 a[i]=((x[i]-x[(i-1)])/6);
 b[i]=((x[i+1]-x[(i-1)])/3);
 c[i]=((x[i+1]-x[(i)])/6);
 f[i]=(((y[i+1]-y[i])/(x[i+1]-x[i]))-(y[i]-y[i-1])/(x[i]-x[i-1]));
 
//For i=n	
 for (i=n; i=n; i=n){
 a[n]=((x[(n-2)]-x[n-1])/6);
 b[n]=((x[n-2]-x[n-1])/3);
 c[n]=0;
 f[n]=((y[n-1]-y[n-2])/(x[n-1]-x[n-2]));

 tridag(a, b, c, f, y); 

 return 0;
 }
 }
 }
 

}
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>

using namespace std;

const double pi = 3.141592654;
double f(double x) {
return 2.0/sqrt(pi)*exp(-(x*x));
}

int main () {
double a, b, c, value1, value2, h, integral1, integral2, trap;
int n;
double acc; // accuracy 
cout << "Please, define the accuracy: ";
cin >> acc;
a = 0.0;
b = 2.0;
n = 2; 
h = b - a;
trap = 0.5*( f(a)+ f(b)); //gap between  unfilled points.
integral1 = h*trap;
c = 0.5*(b+a);
h = c - a;
trap = f(c) + trap;
integral2 = h*trap;
value1 = ((4.0*integral2)-integral1)/3.0;

c = 0.5*(c+a);
h = c-a;
trap = trap + f(c) + f(c+(2*h)); //The next iteration of simpson's rule.
integral1 = integral2;
integral2 = h*trap;
value2 = ((4.0*integral2)-integral1)/3.0; 
int i = 5;

while ((integral2-integral1) > integral1*acc ) {
h = c-a; 
c = 0.5*(c+a); { //midpoint between old c and a.
double size = 2^(n) - 1;
for(int k=0; k < size ; k++){
trap = trap + f(c+(2*k*h));
i++; 
}
integral2 = h*trap;
integral1 = integral2;
value1 = value2;
value2 = ((4.0*integral2)-integral1)/3.0; }
std::cout << value2 << " is the integral" << '\n'; }
return 0;
}

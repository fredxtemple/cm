#include <iostream>
#include <fstream>
#include <time.h>
#include <cmath>
#include <cstdio>
#include <ctime>
#include "mersenne.h" 
using namespace std;
int main()
{
std::clock_t start;
    double duration;

    start = std::clock();

    ofstream myfile;
myfile.open ("histq3new.txt");
MersenneTwister prng(1000);
   for(int i = 0; i <= 100000; i++) { 
   double x = 3.141592654 - 3.141592654*((prng())/4294967295.0); //Range between 0 and pi
   double y = acos(1.0-0.6366197724*x); 
   double cy = 1.570796327*sin(y);
   double z = cy - cy*((prng())/4294967295.0); // z is between 0 and c(y0)
   double pdf = 0.6366197724*(sin(y)*sin(y));
   if ( z <=  pdf ) { //If this condition is true, accept y.
   myfile << y << endl; }
   }
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"printf: "<< duration <<'\n';

return 0;
}
